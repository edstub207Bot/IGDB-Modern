<?php 		
/*DO NOT UPGRADE FONT AWESOME BEYOND 5.8.1 WITHOUT CHANGING SWITCH ICON*/
	
		echo $before_widget;
	if (! empty( $instance['title'] ) ){	echo $before_title . $title . $after_title; }	
?>


<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">



<div class="ts_igdb_out_widget">


	<style>
        .ts_igdb_out_widget li{
            border-bottom: 1px solid #ddd;
			padding: 25px 0px !important;
        }
    	.platform.cat-links,.release_dates .relase_date{
    		background-color: #0b91c1;
			color: #ffffff;
			font-size: 15px;
			padding: 3px 10px;
        	display: inline-block;
        	margin: 0px;
        }


        .release_dates { text-align: center; padding:3px 0px;}
        .release_dates .relase_date{background-color: #0b91c1; width:100%;}
		/*..genres { text-align: center; padding:3px 0px;}
        .genres{background-color: #81d742; width:100%;}*/

        li.cwp-popular-review {
            max-width: 250px;
        }
        img.game-img{
            width: 100%;
            height: 130px;
            max-width: 100%;
        }

        ul.game-list-view{
            list-style: none;
        }
        span.lxt-gamen {    background: #0b91c1;}
        .lxt-game h3 {    color: #FFF;    text-align: center;    width: 100% !important;    text-transform: uppercase;    font-family: Montserrat, "Helvetica Neue", sans-serif;    margin-left: 0px !important;    padding: 15px 15px 0px 15px;}
        span.game-rating {    padding: 12px;    background: #0b91c1;    float: right;    font-size: 12px;    color: #FFF;    font-weight: 500;}
        span.game-sno {   color: #FFF; padding: 14px 17px; font-size: 15px; font-weight: 600; float: left;}
        .lxt-game {
            height: 300px;
            background-size: cover !important;
            background-position: center center !important;
            background-repeat: no-repeat !important;
            max-width: 500px;
            width: 100%;
        }
        /*.gplatform {    display: none;}*/
    </style>
	<?php $this->format_output($api,$filters); ?>
</div>
<?php echo $after_widget; ?>